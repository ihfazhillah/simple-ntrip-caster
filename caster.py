import socketserver
import time
import base64


gnss_data = {}
caster_pw = "testing"
caster_mountpoints = ["test", "hai"]
credentials = ["aku:ihfazh", "dik:dik"]
socket_bind = {}


def generate_sourcetable(mountpoints):
    # TODO: use actual rtcm messages data, lat lon height data :D
    str_template = "STR;{};Indonesia;RTCM3;none;1;none;TEST;IND;40.00;140.00;0;0;receiver5;none;B;N;3600;none"
    template = """HTTP/1.1 200 OK
Server: python-simple-caster 1.0\r
Content-Type: text/plain\r
Content-Length: {}\r\n
{}\r\n
ENDSOURCETABLE\r\n
    """

    strs = "\r\n".join([str_template.format(mountpoint) for mountpoint in mountpoints])

    return template.format(len(strs), strs)


class Request:
    def __init__(self, raw_request):
        self.raw_request = raw_request.decode()
        self.request_line, self.headers_txt = self.raw_request.split("\r\n", 1)
        self.headers = {}

        self.parse_request_line()
        self.parse_headers()

    def parse_request_line(self):
        method, other1, other2 = self.request_line.split()
        self.method = method

        if method == "SOURCE":
            self.password, self.mountpoint = other1, other2
        else:
            self.route, self.httpver = other1, other2

    def parse_headers(self):
        import email

        headers = email.message_from_string(self.headers_txt)
        self.headers = dict(headers)

    def __str__(self):
        return f"{self.method}"


class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def setup(self):
        self.data = self.request.recv(1042)
        self.request_object = Request(self.data)

    def handle(self):

        print(f"request object: {self.request_object}")
        print()
        print(self.request_object.headers)

        for handler_string in dir(self):
            if handler_string.startswith("handle_"):
                handler = getattr(self, handler_string)
                method = handler_string.replace("handle_", "")

                if method == self.request_object.method and handler:
                    handler()

    def handle_SOURCE(self):
        if self.request_object.password != caster_pw:
            self.request.sendall(b"ERROR - Bad Password\r\n")
            return

        if self.request_object.mountpoint not in caster_mountpoints:
            # whats the speks?
            self.request.sendall(b"ERROR - Bad Mountpoint\r\n")
            return

        self.request.sendall(b"ICY 200 OK\r\n")
        socket_bind[self.request_object.mountpoint] = []
        while 1:
            print("get the data")
            new_data = self.request.recv(1024)
            gnss_data[self.request_object.mountpoint] = new_data
            for sock in socket_bind[self.request_object.mountpoint]:
                sock.sendall(new_data)
            if len(new_data) == 0:
                break
            time.sleep(1)
        return

    def handle_GET(self):
        if self.request_object.route == "/":
            sourcetable = generate_sourcetable(caster_mountpoints)
            print(sourcetable)
            print(sourcetable.encode())
            self.request.sendall(sourcetable.encode())
            self.request.close()
            return

        if self.request_object.route == "/sourcetables.txt":
            sourcetable = generate_sourcetable(caster_mountpoints)
            self.request.sendall(sourcetable.encode())
            self.request.close()
            return

        path = self.request_object.route.replace("/", "")
        if path not in caster_mountpoints:
            self.request.sendall(b"SOURCETABLE 200 OK\r\n")
            sourcetable = generate_sourcetable(caster_mountpoints)
            self.request.sendall(sourcetable.encode())
            self.request.close()
            return

        # if no auth
        if "Authorization" not in self.request_object.headers:
            self.request.sendall(b"HTTP/1.0 401 Unauthorized\r\n")
            return

        auth = self.request_object.headers["Authorization"].replace("Basic", "").strip()
        userpass = base64.b64decode(auth).decode()
        if userpass not in credentials:
            print("bad password", userpass)
            self.request.sendall(b"HTTP/1.0 401 Unauthorized\r\n")
            return

        data = gnss_data.get(path)
        if data:
            self.request.sendall(b"ICY 200 OK\r\n")
            socket_bind[path].append(self.request) 
            # self.request.sendall(gnss_data[path])
        else:
            self.request.sendall(b"HTTP/1.0 500\r\n")
        return


class ThreadedTcpServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    allow_reuse_address = True
    pass


if __name__ == "__main__":
    HOST, PORT = "localhost", 2005
    server = ThreadedTcpServer((HOST, PORT), MyTCPHandler)

    with server:
        server.serve_forever()
